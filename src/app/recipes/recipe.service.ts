import { Subject } from 'rxjs';
import { ShoppingListService } from './../shopping-list/shopping-list.service';
import { Recipe } from "./recipe.model";
import { Injectable} from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();

  //  private recipes: Recipe[] = [
  //       new Recipe('Pizza',
  //        'Italian Pizza',
  //         'https://az809444.vo.msecnd.net/image/5370434/0x382/0/recipe-pizza-pollo-arrosto.jpg',
  //         [
  //           new Ingredient('Do',1),
  //           new Ingredient('French Fries',20)
  //         ]),
  //       new Recipe('Burger', 
  //       'Veg Burger', 
  //       'https://www.indianhealthyrecipes.com/wp-content/uploads/2016/02/veg-burger-recipe-1.jpg',
  //       [
  //         new Ingredient('Meat',1),
  //           new Ingredient('French Fries',20)
  //       ])
  //     ];
      private recipes: Recipe[] = [];

      constructor(private slService: ShoppingListService){}

      setRecipes(recipes: Recipe[]) {
        this.recipes = recipes;
        this.recipesChanged.next(this.recipes.slice());
      }

      getRecipe() {
          return this.recipes.slice();
      }

      getRecipes(index: number) {
        return this.recipes[index];
      }

      addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
      }

      addRecipe(recipe: Recipe){
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
      }

      updateRecipe(index: number, newRecipe: Recipe) {
           this.recipes[index] = newRecipe;
           this.recipesChanged.next(this.recipes.slice());
      }

      deleteRecipe(index: number){
        this.recipes.splice(index, 1);
        this.recipesChanged.next(this.recipes.slice());
      }
}